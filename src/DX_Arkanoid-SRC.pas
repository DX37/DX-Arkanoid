program DX_Arkanoid;
uses
        wincrt, wingraph;
const

//���� ������
        Up= #72;
        Down= #80;
        Enter= #13;
        Left= #75;
        Right= #77;
        Esc= #27;
        BackSpace= #8;

//����� � �ਭ� ࠪ�⪨. ����� �� ࠧ�襭�� � ���⨭�� ࠪ�⪨
        D= 120;
        SH= 20;

//��������� ��ப � ���
        window_title= 'DX Arkanoid 0.9.1 RC';
        win= '�஢��� �ன���! =)';
        lose= '�� �ந�ࠫ�! =(';
        main_menu= '-- ������� ���� --';
        select= '����� �஢���';
        about= '�� ���';
        exiting='��室';
        w1= 'DX Games �।�⠢���...';
        w2= '������ ���� �������...';
        winner= '�� ��諨 �� �஢��!';
        selecter= '�롥�� �஢���:';
        New= '����� ���';
        Restore= '�த������ ����';

//�⨫� ⥪�� ����
        TextStyle= 3; //"1" - ���ᥫ�� ����. "3" - ������� ����.

//����� ����㯠 �� ��� ��࠭� ��� �࠭��
        N= 40;

//��ਭ� ���� ����� ��ப��� � �����
        LEN= 20;

//������⢮ �窮�, ����室���� ��� ������
        Level1C= 70;
        Level2C= 59;
        Level3C= 46;

type
        //���ᨢ ��� �뢮�� ��⥬� ��௨祩
        LevelS= array [1..5, 1..20] of integer;
var
        Gdriver, Gmode: integer;
//������� ��६���� ��� ࠡ��� �ணࠬ��
        x, y: integer;
        p1, p2, p3: pointer;
        ch: char;
        x1, y1, hx, hy, r: integer;
        x2, y2, h: integer;
        n1, n2: integer;
        xX, yY, xX1, yY1: integer;
        bx, by: integer;
        x0, y0: integer;
        i, j: integer;
        nx, ny, dx, dy: integer;
        Blocks: LevelS;
        new_game: string;
        level: integer;
        LevelC: integer;
        C: integer;
        hxR, hyR, x1R, y1R, x2R, y2R: integer;
        DE: integer;

//�஢�� ����
        Level1:LevelS=
        ((0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0),
        (0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0),
        (0, 0, 0, 2, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 2, 0, 0, 0),
        (0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 0, 0),
        (0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0));

        Level2:LevelS=
        ((0, 0, 0, 0, 1, 1, 1, 1, 3, 3, 3, 3, 3, 1, 1, 1, 0, 0, 0, 0),
        (0, 0, 1, 1, 1, 1, 1, 1, 3, 2, 3, 2, 3, 1, 1, 1, 1, 1, 0, 0),
        (0, 0, 0, 2, 2, 2, 2, 2, 3, 2, 3, 2, 3, 2, 2, 2, 2, 0, 0, 0),
        (0, 0, 0, 0, 3, 1, 1, 1, 3, 2, 3, 2, 3, 1, 1, 3, 0, 0, 0, 0),
        (0, 0, 0, 0, 0, 1, 1, 1, 3, 3, 3, 3, 3, 1, 1, 0, 0, 0, 0, 0));

        Level3:LevelS=
        ((0, 0, 0, 0, 0, 0, 3, 3, 3, 2, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0),
        (0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0),
        (0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0),
        (0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0),
        (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

//������ ���ᨢ� ��� ॠ����樨
//����⠭������� ��௨祩 ��᫥ �롮� �஢�� ��� �ந����
        Level1R:LevelS=
        ((0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0),
        (0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0),
        (0, 0, 0, 2, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 2, 0, 0, 0),
        (0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 0, 0),
        (0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0));

        Level2R:LevelS=
        ((0, 0, 0, 0, 0, 1, 1, 1, 3, 3, 3, 3, 3, 1, 1, 1, 0, 0, 0, 0),
        (0, 0, 0, 1, 1, 1, 1, 1, 3, 2, 3, 2, 3, 1, 1, 1, 1, 1, 0, 0),
        (0, 0, 0, 0, 2, 2, 2, 2, 3, 2, 3, 2, 3, 2, 2, 2, 2, 0, 0, 0),
        (0, 0, 0, 0, 0, 3, 1, 1, 3, 2, 3, 2, 3, 1, 1, 3, 0, 0, 0, 0),
        (0, 0, 0, 0, 0, 0, 1, 1, 3, 3, 3, 3, 3, 1, 1, 0, 0, 0, 0, 0));

        Level3R:LevelS=
        ((0, 0, 0, 0, 0, 0, 3, 3, 3, 2, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0),
        (0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0),
        (0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0),
        (0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0),
        (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

//��楤�� ��� ������
procedure ResetPos;
begin
        x1:= x1R;
        y1:= y1R;
        hx:= hxR;
        hy:= hyR;
        x2:= x2R;
        y2:= y2R;
        n1:= 0; n2:= 0;
        C:= 0;
end;

//�㭪�� ����㧪� 䠩��� � ������
function Loader (filename: string):pointer;
var
        f: file;
        p: pointer;
        size: longint;
begin
        assign(f, filename);
        reset(f, 1);
        size:= filesize(f);
        getmem(p, size);
        blockread(f, p^, size);
        close(f);
        Loader:= p;
end;

//����㧪� ���⨭�� � ������ �१ �㭪�� Loader
procedure Initpict;
begin
        p1:= Loader('Ball.bmp');
        p2:= Loader('Racket.bmp');
        p3:= Loader('Logo.bmp');
end;

//��楤�� �롮� �஢��. �룫廊� ⠪��, ��� � ����.
procedure SelectLevel;
var
        p, d: integer;
        x, y, xp, x0, y0: integer;
        c: longint;
begin
        cleardevice;

        d:= 50;

        x0:= (getmaxX div 3);
        y0:= (getmaxY div 3);
        x:= (getmaxX div 3);
        y:= y0+d*2;

        xp:= x-d;

        setcolor(lightcyan);
        settextstyle(TextStyle, 0, 6);

        rectangle(N, N, getmaxX-N, getmaxY-N);

        //outtextxy(x0, N+20, w2);
        putimage(x0-40, N+20, p3^, 1);
        outtextxy(x0, y0, selecter);
        outtextxy(x, y+d*0, '1');
        outtextxy(x, y+d*1, '2');
        outtextxy(x, y+d*2, '3');

        p:= 1;

        putimage(xp, y+(p-1)*d, p1^, 1);
        ch:= readkey;
        repeat
                ch:= readkey;
                if ch= #0 then
                        begin
                                ch:= readkey;
                                putimage(xp, y+(p-1)*d, p1^, 1);

                                case ch of
                                        Up: if p > 1 then p:= p-1;
                                        Down: if p < 3 then p:= p+1;
                                end;
                                putimage(xp, y+(p-1)*d, p1^, 1);
                        end;
        until ch= Enter;

        case p of
                1:begin
                        level:= 1;
                        Level1:= Level1R;
                        LevelC:= Level1C;
                        ResetPos;
                  end;
                2:begin
                        level:= 2;
                        Level2:= Level2R;
                        LevelC:= Level2C;
                        ResetPos;
                end;
                3:begin
                        level:= 3;
                        Level3:= Level3R;
                        LevelC:= Level3C;
                        ResetPos;
                end;
        end;
        new_game:= new;
end;

//�㭪�� �஢�ન �⮫��������
function Check(x1, y1, r, x2, y2, dx, dy: integer):boolean;
begin
        if ((y2>y1) and ((y2-y1)<r) and (((x1>x2) and ((x1-x2)<dx))
        or ((x2>x1) and ((x2-x1)<r)))) or ((y1>y2) and
        ((y1-y2)<r+dy) and (((x1>x2) and ((x1-x2)<dx))
        or ((x2>x1) and ((x2-x1<r))))) then Check:= true
        else Check:= false;
end;

//��楤�� ��।������ �஢��
procedure InitLevel(level: integer);
begin
        if (level = 1) then
        Blocks:= Level1;

        if (level = 2) then
        Blocks:= Level2;

        if (level = 3) then
        Blocks:= Level3;
end;

//��楤��, 㭨�⮦���� ��௨�
procedure KillBlock(var hy: integer; nx, ny, dx, dy, r: integer);
var
        x, y, i, j: integer;
begin
        for j:= 1 to 20 do
                for i:= 1 to 5 do
                        begin
                                x:= nx+(j-1)*dx;
                                y:= ny+(i-1)*dy;

                                if (Blocks[i, j] <> 0) and Check(x1, y1, r, x, y, dx, dy)
                                then begin
                                        hy:= -hy;
                                        if Blocks[i, j]= 2 then begin
                                                setfillstyle(1, lightgreen);
                                                bar(x+2, y+2, x+dx-2, y+dy-2);
                                                C:= C+10;
                                                Blocks[i, j]:= 1;
                                                exit;
                                                end;
                                        if Blocks[i, j]= 1 then begin
                                                setfillstyle(1, lightred);
                                                bar(x+2, y+2, x+dx-2, y+dy-2);
                                                C:= C+25;
                                                Blocks[i, j]:= 3;
                                                exit;
                                                end;
                                        if Blocks[i, j]= 3 then begin
                                                setfillstyle(1, black);
                                                bar(x+2, y+2, x+dx-2, y+dy-2);
                                                C:= C+50;
                                                Blocks[i, j]:= 0;
                                                n2:= n2+1;
                                                exit;
                                                end;
                                        exit;
                                     end;
                        end;
        end;

//��楤��, ������ ��௨�
procedure DrawBlocks(nx, ny, dx, dy: integer);
var
        x, y, i, j: integer;
begin
        for j:= 1 to 20 do
                for i:= 1 to 5 do
                        begin
                                x:= nx+(j-1)*dx;
                                y:= ny+(i-1)*dy;
                                case Blocks[i, j] of
                                        1:begin
                                        setfillstyle(1, lightgreen);
                                        bar(x+2, y+2, x+dx-2, y+dy-2);
                                        end;
                                        2:begin
                                        setfillstyle(1, lightcyan);
                                        bar(x+2, y+2, x+dx-2, y+dy-2);
                                        end;
                                        3:begin
                                        setfillstyle(1, lightred);
                                        bar(x+2, y+2, x+dx-2, y+dy-2);
                                        end;
                                end;
                        end;
end;

//��楤�� ��᢮���� ���ᨢ� ���ᮢ�� ��⥬� ��௨祩
//���ᨢ� �஢��
procedure LevelWrite;
begin
        if level = 1 then
                begin
                        Level1:= Blocks;
                        DrawBlocks(nx, ny, dx, dy);
                        LevelC:= Level1C;
                end;

        if level = 2 then
                begin
                        Level2:= Blocks;
                        DrawBlocks(nx, ny, dx, dy);
                        LevelC:= Level2C;
                end;

        if level = 3 then
                begin
                        Level3:= Blocks;
                        DrawBlocks(nx, ny, dx, dy);
                        LevelC:= Level3C;
                end;
end;

//��楤�� ���ࠢ�塞��� ��������
procedure Uncontrolled(var x, y, hx, hy, r: integer; p: pointer);
begin
        putimage(x, y, p^, 1);
        x:= x+hx;
        y:= y+hy;
        if (x <= r) or (x >= getmaxX-r) then hx:= -hx;
        if (y <= r) then hy:= -hy;
        if (y >= getmaxY-r) then
                begin
                        //hy:= -hy;
                        n1:= 1;
                end;
        putimage(x, y, p^, 1);
        delay(DE);
end;

//��楤�� �ࠢ�塞��� ��������
procedure Controlled(var x, y, h: integer; p: pointer);
begin
        ch:= readkey;
        if ch= #0 then
                begin
                        ch:= readkey;
                        putimage(x, y, p^, 1);

                        case ch of
                                Left: if x > h then x:= x-h;
                                Right: if x < getmaxX-140 then x:= x+h;
                        end;
                        putimage(x, y, p^, 1);
                end;
end;

//��楤�� ���� �窮�
procedure Count(n, xw, yw, c: longint);
var
        s: string;
begin
        setfillstyle(1, c);
        bar(xw, yw, xw+200, yw+50);
        setcolor(yellow);
        settextstyle(TextStyle, 0, 6);
        str(n, s);
        outtextXY(xw+2, yw+2, s);
end;

//��楤��, ����� ����᪠�� ����
procedure Game;
begin
        randomize;

        xX:= getmaxX div 2; yY:= getmaxY div 2;
        xX1:= getmaxX div 3; yY1:= getmaxY div 3;
        bx:= getmaxX div 20; by:= 40;
        nx:= getmaxX div 20; ny:= getmaxY div 5;
        dx:= 60; dy:= 25;

        cleardevice;

        InitLevel(level);

        setcolor(lightcyan);
        settextstyle(TextStyle, 0, 6);
        cleardevice;

        //"�����" �� � ࠪ��� � ����
        putimage(x1, y1, p1^, 1);
        putimage(x2, y2, p2^, 1);

        //�������� ��������
        repeat
        Uncontrolled(x1, y1, hx, hy, r, p1);
        if keypressed then Controlled(x2, y2, h, p2);

        //���� �窮�
        Count(C, 10, 10, green);

        //�࠭�� ��஢��� ����
        Line(0, 70, getmaxX, 70);

        LevelWrite;

        //�஢�ઠ �� ���⠪� � �࠭�楩
        if y1 <= 70 then
                begin
                        hy:= -hy;
                end;

        //�஢�ઠ ��᪮��
        if ((y2>y1) and ((y2-y1)<r) and (((x1>x2) and ((x1-x2)<D)) or ((x2>x1) and ((x2-x1)<r)))) or ((y1>y2) and ((y1-y2)<r+SH) and (((x1>x2) and ((x1-x2)<D) or ((x2>x1) and ((x2-x1)<r)))))
                then begin
                        hy:= -hy;
                end;

        //�᫨ ��ப 㭨�⮦�� �� ��௨� - ������
        if n2 >= LevelC then
                begin
                        setcolor(lightcyan);
                        settextstyle(TextStyle, 0, 6);
                        OutTextXY(xX1, yY, win);
                        delay(1500);

                        level:= level+1;
                        if level = 4 then
                                begin
                                        cleardevice;
                                        outtextXY(xX1, yY, winner);
                                        delay(1500);
                                        level:= 1;
                                        Level1:= Level1R;
                                        Level2:= Level2R;
                                        Level3:= Level3R;
                                        ResetPos;
                                        break;
                                end;
                        ResetPos;
                        cleardevice;
                        break;
                end;

        //�᫨ ��ப �ய��⨫ ��� - �ந����
        if n1 = 1 then
                begin
                        setcolor(lightcyan);
                        settextstyle(TextStyle, 0, 6);
                        OutTextXY(xX1, yY, lose);
                        delay(1500);
                        cleardevice;
                        ResetPos;

                        if level = 1 then
                                begin
                                        Level1:= Level1R;
                                        DrawBlocks(nx, ny, dx, dy);
                                        LevelC:= Level1C;
                                end;

                        if level = 2 then
                                begin
                                        Level2:= Level2R;
                                        DrawBlocks(nx, ny, dx, dy);
                                        LevelC:= Level2C;
                                end;

                        if level = 3 then
                                begin
                                        Level3:= Level3R;
                                        DrawBlocks(nx, ny, dx, dy);
                                        LevelC:= Level3C;
                                end;
                        new_game:= new;
                        break;
                end;

        KillBlock(hy, nx, ny, dx, dy, r);

        until ch= esc;

        new_game:= restore;

        cleardevice;
end;

//��楤�� �⮡ࠦ���� ᮤ�ন���� 䠩�� help.pas
//� �㭪� About �������� ���� ����
procedure Help;
var
        f: text;
        s: string;
        y: integer;
begin
        assign(f, 'help-ru.pas');
        reset(f);
        setcolor(lightcyan);
        settextstyle(TextStyle, 0, 4);
        y:= LEN;
        cleardevice;
        while not(eof(f)) do
                begin
                        readln(f, s);
                        outtextxy(50, y, s);
                        y:= y+LEN;
                end;
        close(f);
        readkey;
        cleardevice;
end;

//��楤�� ᮧ����� �������� ���� ����
procedure Menu;
var
        p, d: integer;
        x, y, xp, x1, y1: integer;
        c: longint;
begin
        cleardevice;

        d:= 50;

        x0:= (getmaxX div 3);
        y0:= (getmaxY div 3);
        x:= (getmaxX div 3);
        y:= y0+d*2;

        xp:= x-d;

        setcolor(lightcyan);
        settextstyle(TextStyle, 0, 6);

        rectangle(N, N, getmaxX-N, getmaxY-N);

        //outtextxy(x0, N+20, w2);
        putimage(x0-40, N+20, p3^, 1);
        outtextxy(x0, y0, main_menu);
        outtextxy(x, y+d*0, new_game);
        outtextxy(x, y+d*1, select);
        outtextxy(x, y+d*2, about);
        outtextxy(x, y+d*3, exiting);

        p:= 1;

        putimage(xp, y+(p-1)*d, p1^, 1);
        ch:= readkey;
        repeat
                ch:= readkey;
                if ch= #0 then
                        begin
                                ch:= readkey;
                                putimage(xp, y+(p-1)*d, p1^, 1);

                                case ch of
                                        Up: if p > 1 then p:= p-1;
                                        Down: if p < 4 then p:= p+1;
                                end;

                                putimage(xp, y+(p-1)*d, p1^, 1);
                        end;
        until ch= Enter;

        case p of
                1:Game;
                2:SelectLevel;
                3:Help;
                4:halt;
        end;
end;

//���⠢��, �ਢ���⢨�
procedure Intro;
begin
        cleardevice;

        putimage(getmaxX div 3, getmaxY div 3, p3^, 1);
        setcolor(lightred);
        settextstyle(TextStyle, 0, 6);
        delay(2000);
        outtextxy(getmaxX div 3, (getmaxY div 3)+150, w2);

        readkey;
end;

//����� � ���᮫�
procedure Lined;
begin
        writeln('==============================================');
end;

BEGIN
        randomize;

        Lined;
        writeln('���� ���������� � ���� ',window_title);
        Lined;
        writeln('����室��� ����� ����প� ࠡ���');
        writeln('�������� ��. ������ �� ��������.');
        writeln('��� ᫠��� ��������, ⥬ ������� ����প�');
        writeln('����室��� �������. ������ - 1.');
        writeln('�� �������� ����讥 �᫮.');
        Lined;
        write('������ ����প�: ');
        readln(DE);

//���樠������ ��䨪� � ������ ���� � ����⠭� Window_Title
        Gdriver:= D4Bit;
        Gmode:= mFullScr;
        Initgraph(Gdriver, Gmode, Window_Title);

//����㧪� ���⨭��, ���ᠭ��� � ��楤�� Initpict
        Initpict;

//��ᢠ����� ����室��� ��६���� �� ��室�� ���祭��
        n1:= 0; n2:= 0;
        C:= 0;
        level:= 1;
        h:= 20;
        new_game:= new;
        r:= 40; hx:= 4; hy:= 4;
        x1:= getmaxX div 3; y1:= getmaxY div 2;
        x2:= (getmaxX div 2)-50; y2:= getmaxY-50;

//������ ��६����, ��� ॠ����樨 ��� ����権 � �.�.
        hxR:= 4; hyR:= 4;
        x1R:= getmaxX div 3; y1R:= getmaxY div 2;
        x2R:= (getmaxX div 2)-50; y2R:= getmaxY-50;

//�⮡ࠦ���� ���⠢��
        Intro;

//�⮡ࠦ���� ���� � ��᪮��筮� 横��
        repeat
                Menu;
        until 1=2;

        readln;
END.
